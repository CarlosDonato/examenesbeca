use BASE;
create table Servidores (
    ServID int  not null primary key identity(1,1),
    ServName varchar(255),
    ServCountry varchar(255),
    AvgMS int,
    Game varchar(255),
);