﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Servers.Models
{
    public class ServersModel
    {
            [Key]
            public int ServID { get; set; }

            [Required(ErrorMessage = "Ingrese el nombre del servidor")]
            [Display(Name = "Servidor")]
            public string ServName { get; set; }

            [Required(ErrorMessage = "Ingrese nombre del País")]
            [Display(Name = "País")]
            public string ServCountry { get; set; }

            [Required(ErrorMessage = "Ingrese el ping entre 10 y 600")]
            [Display(Name = "Ping promedio")]
            [Range(10, 600)]
            public int AvgMS { get; set; }

            [Required(ErrorMessage = "Ingresa el TAG")]
            [Display(Name = "TAG de País")]
            [StringLength(2)]
            public string Game { get; set; }
    }
    
}
