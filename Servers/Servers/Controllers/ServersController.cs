﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Servers.Models;

namespace Servers.Controllers
{
    public class ServersController : Controller
    {
        private readonly ApplicationDbContext _db;

        public ServersController(ApplicationDbContext db)
        {
            _db = db;
        }

        public IActionResult Index()
        {
            var displaydata = _db.Servidores.ToList();
            return View(displaydata);
        }
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(ServersModel nServ)
        {
            if (ModelState.IsValid)
            {
                _db.Add(nServ);
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(nServ);
        }
        public async Task<IActionResult> Detail(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }
            var getEmpDdetail = await _db.Servidores.FindAsync(id);
            return View(getEmpDdetail);
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }
            var getEmpDdetail = await _db.Servidores.FindAsync(id);
            return View(getEmpDdetail);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(ServersModel oldEmp)
        {
            if (ModelState.IsValid)
            {
                _db.Update(oldEmp);
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(oldEmp);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }
            var getEmpDdetail = await _db.Servidores.FindAsync(id);
            return View(getEmpDdetail);
        }

        [HttpPost]
        public async Task<IActionResult> Delete(int id)
        {
            var getEmpDdetail = await _db.Servidores.FindAsync(id);
            _db.Servidores.Remove(getEmpDdetail);
            await _db.SaveChangesAsync();
            return RedirectToAction("Index");
        }
    }
}
