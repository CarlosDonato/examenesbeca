﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using REST_Servers.Models;

namespace REST_Servers.Controllers
{
    public class ServidoresController : ApiController
    {
        private BASEEntities db = new BASEEntities();

        // GET: api/Servidores
        public IQueryable<Servidores> GetServidores()
        {
            return db.Servidores;
        }

        // GET: api/Servidores/5
        [ResponseType(typeof(Servidores))]
        public IHttpActionResult GetServidores(int id)
        {
            Servidores servidores = db.Servidores.Find(id);
            if (servidores == null)
            {
                return NotFound();
            }

            return Ok(servidores);
        }

        // PUT: api/Servidores/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutServidores(int id, Servidores servidores)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != servidores.ServID)
            {
                return BadRequest();
            }

            db.Entry(servidores).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ServidoresExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Servidores
        [ResponseType(typeof(Servidores))]
        public IHttpActionResult PostServidores(Servidores servidores)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Servidores.Add(servidores);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = servidores.ServID }, servidores);
        }

        // DELETE: api/Servidores/5
        [ResponseType(typeof(Servidores))]
        public IHttpActionResult DeleteServidores(int id)
        {
            Servidores servidores = db.Servidores.Find(id);
            if (servidores == null)
            {
                return NotFound();
            }

            db.Servidores.Remove(servidores);
            db.SaveChanges();

            return Ok(servidores);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ServidoresExists(int id)
        {
            return db.Servidores.Count(e => e.ServID == id) > 0;
        }
    }
}