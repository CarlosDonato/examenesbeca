-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 30-07-2020 a las 23:56:33
-- Versión del servidor: 10.1.26-MariaDB
-- Versión de PHP: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `Servidores`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servs`
--

CREATE TABLE `servs` (
  `id_serv` int(11) NOT NULL,
  `nombre` varchar(80) NOT NULL,
  `Velocidad` int(11) NOT NULL,
  `Region` varchar(25) NOT NULL,
  `Juego` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `servs`
--

INSERT INTO `servs` (`id_serv`, `nombre`, `Velocidad`, `Region`, `Juego`) VALUES
(1, 'Brazil', 150, 'LATAM', 'Leage of Legends'),
(2, 'Mexico', 80, 'LATAM NA', 'Overwatch'),
(3, 'EspaÃ±a', 20, 'EU', 'Fortnite');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `servs`
--
ALTER TABLE `servs`
  ADD PRIMARY KEY (`id_serv`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `servs`
--
ALTER TABLE `servs`
  MODIFY `id_serv` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
