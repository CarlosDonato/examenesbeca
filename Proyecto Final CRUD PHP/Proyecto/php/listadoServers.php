<?php
// inclir archivo de conexion
  include("conexion.php");

  // crear consulta para recargar datos
    $consulta = "SELECT
                  id_serv,
                  nombre,
                  Velocidad,
                  Region,
                  Juego
                  FROM servs";

    $ejecuta = $conexion -> query($consulta) or die("Error al consultar lista de servers <br> :". $conexion -> error);

?>
<table id="lista_servers">
	<tr>
		<th>ID</th>
		<th>Nombre</th>
		<th>Velocidad</th>
		<th>Región</th>
		<th>Juego</th>
    <th colspan="2">Edicion</th>
	</tr>
<?php

    while($arreglo_resultados = $ejecuta -> fetch_row()){
    echo '<tr>';
		echo '<td>' . $arreglo_resultados[0] . '</td>';
		echo '<td>' . $arreglo_resultados[1] . '</td>';
		echo '<td>' . $arreglo_resultados[2] . ' ms</td>';
		echo '<td>' . $arreglo_resultados[3] . '</td>';
		echo '<td>' . $arreglo_resultados[4] . '</td>';
    //boton para editar
    echo '<td> <button type="button" onclick="formServer('.$arreglo_resultados[0].');">';
  		echo 'Editar</button></td>';
  	//boton para eliminar
  		echo '<td> <button type="button" onclick="eliminarServer('.$arreglo_resultados[0].');">';
  		echo 'Eliminar</button></td>';
  		echo '</tr>';
  	}
?>
</table>
